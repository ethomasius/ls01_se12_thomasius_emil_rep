import java.util.Scanner;
public class Arrays {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    int [] intArray = new int [5];
    int [] zweitesArray = {3,7,334,567,2356,3456};
      
    int index, wert;
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    intArray[2]  = 1000;
    intArray[4] = 500;
    
    /*  
    System.out.println("Index 0: " + intArray[0]);  
    System.out.println("Index 1: " + intArray[1]); 
    System.out.println("Index 2: " + intArray[2]); 
    System.out.println("Index 3: " + intArray[3]); 
    System.out.println("Index 4: " + intArray[4]); 
    */
    
    //Array ausgeben mit For-Schleife
    for (int i = 0; i < intArray.length; i++) {
      System.out.println("Index  " + i + ": "+ intArray[i]); 
    }
    
 
    
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index soll der neue Wert?");
     index = sc.nextInt();
    
    System.out.println("Geben Sie den neuen Wert f�r index " +   index   + " an:");
     wert = sc.nextInt();
    
    intArray[index] = wert;
    
           for (int i = 0; i < intArray.length; i++) {
      System.out.println("Index  " + i + ": "+ intArray[i]); 
    } 
    
    //Der intArray soll mit neuen Werten gef�llt werden
    //1. alle Felder sollen den Wert 0 erhalten
    
    for (int i = 0; i < intArray.length; i++) {
        intArray[i] = 0; 
    }

       for (int i = 0; i < intArray.length; i++) {
      System.out.println("Index  " + i + ": "+ intArray[i]); 
    } 
    

    //Der intArray soll mit neuen Werten gef�llt werden
   //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    int x = 10;
     for (int i = 0; i < intArray.length; i++) {
        intArray[i] = x;
        x = x+10; 
    }
    for (int i = 0; i < intArray.length; i++) {
      System.out.println("Index  " + i + ": "+ intArray[i]); 
    } 
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrungLoesung

