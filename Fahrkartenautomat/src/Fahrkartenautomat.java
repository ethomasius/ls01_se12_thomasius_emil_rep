﻿import java.util.Scanner;
import java.io.Reader;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       String ticketname[] = {
           "Tagesticket",
           "Erwachsenenticket",
           "Fahrradticket",
           "Kinderticket",
           "Kurzstrecke",
           "Tierkarte"
           };
       
      
       
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfenesGeld;
       float rückgabebetrag;
       
       
       float ticket[] = {
           10,      //0 = Tagesticket
           5,     //1 = Erwachsenenticket
           2.50f,   //2 = Fahrradticket
           3,     //3 = Kinderticket
           1.80f,   //4 = Kurzstrecke
           2
       };
       
       int auswahl;
       
       boolean loopAuswahl = true;
       boolean loopMünze = true;
       boolean loopMünzeRichtig = true;
       boolean echtgeldChecker;
       
       do {
       System.out.println("----- [Fahrkartenautomat] -----\n ");
       
       for(int i = 0; i < ticket.length; i++){
           System.out.printf(i+1 + ". %s %.2f€\n" , ticketname[i], ticket[i]);
          }
       
       System.out.println("\n0. Abbrechen\n");
       
       System.out.print("\nWelches Ticket möchten sie kaufen? : ");
       auswahl = tastatur.nextInt();
       

       if(auswahl == 0) {
         System.out.println("Fahrkartenautomat geschlossen!");
         System.exit(0);
       };
       
       if(auswahl >= ticket.length+1 || auswahl <=1) {
         System.out.print("[ERROR] - Eingabe nicht im System enthalten. Vorgang wurde neugestartet\n\n\n\n\n\n\n");
         loopAuswahl = true;
       }
       
       else loopAuswahl = false;
       
       } while (loopAuswahl);
       
       loopAuswahl = true;
       
       float gewaehltesTicketPreis = ticket[auswahl - 1];
       String gewaehltesTicketName = ticketname[auswahl - 1];
       
       zuZahlenderBetrag = gewaehltesTicketPreis;
       System.out.printf("Das " + gewaehltesTicketName + " kostet %.2f€.", gewaehltesTicketPreis);
       
       System.out.println("\nWieviele Tickets davon möchten sie haben?");
       int ticketAnzahl = tastatur.nextInt();
       
       float ticketPreis = ticket[auswahl-1];
       
       zuZahlenderBetrag = Fahrkartenautomat.ticketAnzahlZusammenrechnen(ticketAnzahl, ticketPreis);
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00f;
       
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
         float nochZuZahlenBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
         System.out.printf("\nNoch zu zahlen: %.2f€\n", nochZuZahlenBetrag);
         System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
         eingeworfenesGeld = tastatur.nextFloat();
         
         
         
         echtgeldChecker = Fahrkartenautomat.echtgeldCheck(eingeworfenesGeld);
         
         eingezahlterGesamtbetrag += eingeworfenesGeld;
         
       }
       
       
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
      Thread.sleep(250);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       

       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

       Fahrkartenautomat.rueckgeldRechnung(rückgabebetrag);

      }

    
    public static float echteMünze(float a) {
    
      float münzen[] = {
           0.05f,
           0.10f,
           0.20f,
           0.50f,
           1,
           2
        };
      
      boolean echt = true;
      float fehler = 0.0f;
    
    while(echt) {
      if(a != münzen[0]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }
        
      if(a != münzen[1]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }
      if(a != münzen[2]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }
      
      if(a != münzen[3]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }
      
      if(a != münzen[4]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }
      
      if(a != münzen[5]) {
        echt = true;
      } else {
        echt = false;
        return a;
      }

    return fehler;
      
    }
    return a;
    
    }
 
    public static float echterSchein(float eingeworfenesGeld) {
      
      int scheine[] = {
        5,
        10,
        20,
        50,
      };
      
      boolean echt = true;
      int fehler = 0;
    
    while(echt) {
      if(eingeworfenesGeld != scheine[0]) {
        echt = true;
      } else {
        echt = false;
        return eingeworfenesGeld;
      }
        
      if(eingeworfenesGeld != scheine[1]) {
        echt = true;
      } else {
        echt = false;
        return eingeworfenesGeld;
      }
      
      if(eingeworfenesGeld != scheine[2]) {
        echt = true;
      } else {
        echt = false;
        return eingeworfenesGeld;
      }
      
      if(eingeworfenesGeld != scheine[3]) {
        echt = true;
      } else {
        echt = false;
        return eingeworfenesGeld;
      }

    return fehler;

      }
  return eingeworfenesGeld;
    }
    
    public static boolean echtgeldCheck(float eingeworfenesGeld) {
      
      boolean status = true;
      
      float münzen = Fahrkartenautomat.echteMünze(eingeworfenesGeld);
      
      
      float scheine = Fahrkartenautomat.echterSchein(eingeworfenesGeld);
      
      if (münzen == 0) {
        status = false;
      }
      
      if (scheine == 0) {
        status = false;
      }
      
      if(status == false) System.out.println("Geld nicht erkannt!");
      
      return status;
    }
    
    public static float ticketAnzahlZusammenrechnen(float anzahl, float preis) {
      float produkt = anzahl * preis;
      
      return produkt;
    }

    public static float rueckgeldRechnung(float a) {
      if(a > 0.00)
        {
         System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", a);
         System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(a >= 2.0) // 2 EURO-Münzen
            {
            System.out.println("2 EURO");
            a -= 2.0;
            }
            while(a >= 1.0) // 1 EURO-Münzen
            {
            System.out.println("1 EURO");
            a -= 1.0;
            }
            while(a >= 0.5) // 50 CENT-Münzen
            {
            System.out.println("50 CENT");
            a -= 0.5;
            }
            while(a >= 0.2) // 20 CENT-Münzen
            {
            System.out.println("20 CENT");
              a -= 0.2;
            }
            while(a >= 0.1) // 10 CENT-Münzen
            {
            System.out.println("10 CENT");
            a -= 0.1;
            }
            while(a >= 0.05)// 5 CENT-Münzen
            {
            System.out.println("5 CENT");
              a -= 0.05;
            }
        }


        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n\n\n\n\n");
      
      return a;
      
    }
    
}