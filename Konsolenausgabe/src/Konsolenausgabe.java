
public class Konsolenausgabe {

	public static void main(String[] args) {
		
	// TODO Auto-generated method stub
		//Teil 1
		System.out.print("Dies ist Satz 1! ");// Print ohne Zeilenumbruch.
		System.out.print("Dies ist Satz 2!\n");// Println mit Zeilenumbruch.
		
		//Teil 2
		System.out.println("Dies ist \"Satz 1\"! "); //Mit Operator.
		System.out.println("Dies ist Satz 2!");
	
		
		
		
		//Aufgabe 2
		String no;
		no = "*";
		
		System.out.printf("\n%9s", no); // 1
		System.out.printf("\n%10s", no+no+no); // 2
		System.out.printf("\n%11s", no+no+no+no+no); // 3
		System.out.printf("\n%12s", no+no+no+no+no+no+no); // 4
		System.out.printf("\n%13s", no+no+no+no+no+no+no+no+no); // 5
		System.out.printf("\n%14s", no+no+no+no+no+no+no+no+no+no+no); // 6
		System.out.printf("\n%15s", no+no+no+no+no+no+no+no+no+no+no+no+no); // 7
		System.out.printf("\n%10s", no+no+no); // 8
		System.out.printf("\n%10s\n", no+no+no); // 9
		
		
		//Aufgabe 3
		
		double zahl1, zahl2, zahl3, zahl4, zahl5;
		zahl1 = 22.4234234;
		zahl2 = 111.2222;
		zahl3 = 4.0;
		zahl4 = 1000000.551;
		zahl5 = 97.34;
		
		System.out.printf("%.2f\n", zahl1); //Zahl 1 etc.
		System.out.printf("%.2f\n", zahl2);
		System.out.printf("%.2f\n", zahl3);
		System.out.printf("%.2f\n", zahl4);
		System.out.printf("%.2f\n", zahl5);
		
		
		
		

	}
}
