import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        //1.Programmhinweis
       Addition.hinweis();
       
        //4.Eingabe
      zahl1 = Addition.eingabe1(zahl1, "1. Zahl eingeben");
      zahl2 =  Addition.eingabe1(zahl2,"2. Zahl eingeben"); 

        //3.Verarbeitung
        erg = Addition.verarbeitung(zahl1,zahl2);

        //2.Ausgabe 
        Addition.ausgabe(zahl1, zahl2, erg);
    }
    public static void hinweis() {
    	 System.out.println("Hinweis: ");
         System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }

         public static void ausgabe( double zahl1 , double zahl2, double erg) {
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
}
         public static double verarbeitung(double zahl1, double zahl2) {
        		double a = zahl1 + zahl2;
        		return a;
         }
         public static double eingabe1(double neu, String text) {
        	 System.out.println(text);
             neu = sc.nextDouble();
     		return neu;
         
         }
}